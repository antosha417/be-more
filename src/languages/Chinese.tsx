import { ChineseExercise, ChinesePhrase, Exercise, Language, Phrase } from "../contracts";
import { chinesePhrases, chineseWords } from "../samples/chineseSamples";
import { randomSamples } from "../samples/samples";
import { Card } from "react-bootstrap";
import React from "react";
import Hieroglyph from "../components/exercises/text/Hieroglyph";

export default class Chinese {
  // todo fix
  static exercises: ChineseExercise[] = ['Cards', 'Pinyin', 'Original'];
  static wordsMap = chineseWords;
  static words = Object.values(chineseWords);
  static phrasesMap = chinesePhrases;
  static groups = Object.values(chineseWords).reduce<string[]>((xs, x) => {
    if (!xs.includes(x.group)) {
      xs.push(x.group)
    }
    return xs
  }, [])
  static dir = 'rtl';
  static textAlign: 'left' = 'left'

  static isAvailable(exercise: Exercise) {
    return this.exercises.some(e => e === exercise)
  }

  static isChinesePhrase(phrase: Phrase): phrase is ChinesePhrase {
    return (phrase as ChinesePhrase).pinyin !== undefined;
  }

  static isChineseLanguage(language: Language) {
    return language === 'Chinese';
  }

  static randomSamples(activeGroup: string, showSentences: boolean, groupSize: number): ChinesePhrase[] {
    return randomSamples('Chinese', activeGroup, showSentences, false, Object.values(chineseWords), chineseWords, chinesePhrases, groupSize)
  }

  static showInCard(phrase: Phrase, input: JSX.Element): JSX.Element {
    if (!this.isChinesePhrase(phrase)) return <></>
    return <>
      <Hieroglyph hieroglyph={phrase.original}/>
      {input}
      <Card.Text>{phrase.pinyin}</Card.Text>
      <Card.Text>{phrase.translate}</Card.Text>
    </>
  }
}
