import React from 'react'
import { Exercise, HebrewExercise, HebrewPhrase, Phrase } from "../contracts";
import { hebrewPhrases, hebrewWords } from "../samples/hebrewSamples";
import { Card } from 'react-bootstrap';
import { randomSamples } from "../samples/samples";
import HebrewText from "../components/exercises/text/HebrewText";

export default class Hebrew {
  static exercises: HebrewExercise[] = ['Cards', 'Original', 'List'];
  static wordsMap = hebrewWords
  static words = Object.values(hebrewWords)
  static phrasesMap = hebrewPhrases
  static groups = Object.values(hebrewWords).reduce<string[]>((xs, x) => {
    if (!xs.includes(x.group)) {
      xs.push(x.group)
    }
    return xs
  }, [])
  static dir = 'rtl'
  static textAlign: 'right' = 'right'

  static isAvailable(exercise: Exercise) {
    return this.isHebrewExercise(exercise)
  }

  static isHebrewExercise(exercise: Exercise): exercise is HebrewExercise {
    return this.exercises.some(e => e === exercise)
  }

  static isHebrewPhrase(phrase: Phrase): phrase is HebrewPhrase {
    return (phrase as HebrewPhrase).vowel !== undefined;
  }

  static randomSamples(activeGroup: string, showSentences: boolean, groupSize: number): HebrewPhrase[] {
    return randomSamples('Hebrew', activeGroup, showSentences, true, Object.values(this.words), this.wordsMap, this.phrasesMap, groupSize)
  }

  static showInCard(phrase: Phrase, input: JSX.Element) {
    if (!this.isHebrewPhrase(phrase)) return <></>
    return <>
      <HebrewText hebrew={phrase.original}/>
      {input}
      <Card.Text>{phrase.vowel}</Card.Text>
      <Card.Text>{phrase.translate}</Card.Text>
    </>
  }
}