import { Language } from "../contracts";
import Chinese from "./Chinese";
import Hebrew from "./Hebrew";

export const getByName = (language: Language) => {
  return language === 'Chinese' ? Chinese : Hebrew
}