import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css"
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import BeMore from './BeMore';
import AddChinese from './add/AddChinese';
import AddHebrew from "./add/AddHebrew";

function App() {
  return <>
    <Router>
        <Switch>
          <Route path="/add/chinese">
            <AddChinese />
          </Route>
          <Route path="/add/hebrew">
            <AddHebrew />
          </Route>
          <Route path="/">
            <BeMore />
          </Route>
        </Switch>
    </Router>
    </>
}

export default App;
