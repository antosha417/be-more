import React from 'react';
import { Card } from 'react-bootstrap';


function Hieroglyph({ hieroglyph }: { hieroglyph: string }) {
  const onClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    const selection = window?.getSelection()?.toString();
    if (e.ctrlKey && selection) {
      window.open("https://bkrs.info/slovo.php?ch=" + selection, "bkrs")?.focus();
    }
  }

  return (
    <Card.Title onClick={onClick}>{hieroglyph}</Card.Title>
  )
}

export default Hieroglyph
