import React from 'react';
import { Card } from 'react-bootstrap';


//todo merge with Hierogliph
function HebrewText({ hebrew }: { hebrew: string }) {
  const onClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    const selection = window?.getSelection()?.toString();
    if (e.ctrlKey && selection) {
      window.open("https://he.wiktionary.org/wiki/" + selection, "wiki")?.focus();
    }
  }

  return (
    <Card.Title onClick={onClick}>{hebrew}</Card.Title>
  )
}

export default HebrewText
