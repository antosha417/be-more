import React from 'react'
import { ProgressBar } from "react-bootstrap";

function Progress(props: { groupSize: number, samplesLen: number }) {
  const {groupSize, samplesLen} = props;
  const wordsLeftToStudy: number = groupSize - samplesLen + 1;
  return <ProgressBar now={wordsLeftToStudy / groupSize * 100} label={wordsLeftToStudy + '/' + groupSize}/>
}

export default Progress