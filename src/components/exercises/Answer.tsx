import React from 'react';
import { CardState, Exercise, Phrase } from '../../contracts';
import Chinese from "../../languages/Chinese";

const getExpectedInput = (phrase: Phrase, exercise: Exercise) => {
  let expectedInput = '';
  if (exercise === 'Pinyin' && Chinese.isChinesePhrase(phrase)) {
    expectedInput = phrase.pinyin;
  } else if (exercise === 'Original') {
    expectedInput = phrase.original;
  }
  return expectedInput;
}

const onlyLetters = (s: string): string => s.replace(/[\s,.!?"']+/g, '')

export function isAnswerCorrect(input: string, phrase: Phrase, exercise: Exercise) {
  const expected = getExpectedInput(phrase, exercise);
  return onlyLetters(input) === onlyLetters(expected);
}

function Answer(props: CardState) {
  // const {input, phrase, inProgress} = props;
  // const {original, translate} = phrase;
  //
  // const isCorrect = isAnswerCorrect(input, phrase, inProgress);
  return <>
    {/*{Chinese.isChinesePhrase(phrase) ? <Hieroglyph hieroglyph={original}/> : <HebrewText hebrew={original}/>}*/}

    {/*{(inProgress === 'Original' || inProgress === 'Pinyin' || inProgress === 'Audio') &&*/}
    {/*<Card.Text className={isCorrect ? 'correct' : 'wrong'}>*/}
    {/*  {input || 'Empty'}*/}
    {/*</Card.Text>*/}
    {/*}*/}
    {/*{Chinese.isChinesePhrase(phrase) && <Card.Text>{phrase.pinyin}</Card.Text>}*/}
    {/*{Hebrew.isHebrewPhrase(phrase) && <Card.Text>{phrase.vowel}</Card.Text>}*/}
    {/*<Card.Text>{translate}</Card.Text>*/}

    {/*{inProgress === 'Cards' &&*/}
    {/*<ButtonGroup className="mr-2">*/}
    {/*    <Button variant="primary" onClick={props.studyAgain}>Study again</Button>*/}
    {/*</ButtonGroup>*/}
    {/*}*/}
    <p>Answer</p>
  </>
}

export default Answer
