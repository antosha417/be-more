import React from 'react';
import PinyinInput from '../input/PinyinInput'
import { CardState, Language, Phrase } from '../../../contracts';
import Hieroglyph from '../text/Hieroglyph';
import { Card } from "react-bootstrap";
import { getByName } from "../../../languages/provider";
import DefaultExercise from "./DefaultExercise";
import Chinese from "../../../languages/Chinese";

const Exercise = (props: { phrase: Phrase, setInput(input: string): void, input: string, language: Language, checkPhrase(): void, visible: boolean }) => {
  if (!props.visible || !Chinese.isChineseLanguage(props.language)) return <></>
  if (!Chinese.isChinesePhrase(props.phrase)) return <></>
  return <>
    <Hieroglyph hieroglyph={props.phrase.original} />
    <PinyinInput {...props} />
  </>
}

const onlyLetters = (s: string): string => s.replace(/[\s,.!?"']+/g, '')

const Answer = (props: { phrase: Phrase, input: string, language: Language, visible: boolean }) => {
  if (!props.visible || !Chinese.isChinesePhrase(props.phrase)) return <></>
  const isCorrect = onlyLetters(props.input) === onlyLetters(props.phrase.pinyin);
  const input = <Card.Text className={isCorrect ? 'correct' : 'wrong'}>
    {props.input || 'Empty'}
  </Card.Text>
  return getByName(props.language).showInCard(props.phrase, input)
}

function Pinyin(props: CardState) {
  return <DefaultExercise {...props} exercise={Exercise} answer={Answer}/>
}

export default Pinyin
