import React, { useEffect } from 'react';
import { Card, FormControl, InputGroup } from 'react-bootstrap';
import { CardState, Language, Phrase } from '../../../contracts';
import { getByName } from "../../../languages/provider";
import DefaultExercise from "./DefaultExercise";
import { saveScoreInLocalStorage } from "../../../utils/statistics";


const Exercise = (props: { phrase: Phrase, setInput(input: string): void, input: string, language: Language, checkPhrase(): void, visible: boolean }) => {
  if (!props.visible) return <></>
  return <>
    <Card.Title>{props.phrase.translate}</Card.Title>
    <InputGroup className="mb-3">
      <FormControl
        size="lg"
        type="text"
        placeholder="translation"
        onChange={(e: any) => props.setInput(e.target.value)}
        onKeyPress={(e: any) => {
          e.key === 'Enter' && props.checkPhrase()
        }}
        value={props.input}
        dir={getByName(props.language).dir}
        autoFocus={true}
        autoComplete='off'
        autoCorrect='off'
      />
    </InputGroup>
  </>
}

const onlyLetters = (s: string): string => s.replace(/[\s,.!?"']+/g, '')

const Answer = (props: { phrase: Phrase, input: string, language: Language, visible: boolean }) => {
  const isCorrect = onlyLetters(props.input) === onlyLetters(props.phrase.original);

  useEffect(() => {
    if (!props.visible) return
    saveScoreInLocalStorage(props.language, props.phrase.id || '', isCorrect);
  }, [props.phrase, isCorrect, props.language, props.visible])

  if (!props.visible) return <></>
  const input = <Card.Text className={isCorrect ? 'correct' : 'wrong'}>
    {props.input || 'Empty'}
  </Card.Text>
  return getByName(props.language).showInCard(props.phrase, input)
}

function Original(props: CardState) {
  return (
    <DefaultExercise {...props} exercise={Exercise} answer={Answer}/>
  )
}

export default Original
