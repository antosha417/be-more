import React from 'react';
import { Card } from 'react-bootstrap';
import { CardState, Language, Phrase } from '../../../contracts';
import DefaultExercise from "./DefaultExercise";
import { getByName } from "../../../languages/provider";
import StudyAgainButton from "../buttons/StudyAgainButton";

const Exercise = (props: { phrase: Phrase, visible: boolean}) => {
  if (!props.visible) return <></>
  const {phrase} = props
  return <Card.Title> {phrase.translate} </Card.Title>
}

const Answer = (props: { phrase: Phrase, input: string, language: Language, studyAgain(): void, visible: boolean}) => {
  if (!props.visible) return <></>
  return <>
    {getByName(props.language).showInCard(props.phrase, <></>)}
    <StudyAgainButton {...props}/>
  </>
}

function Cards(props: CardState) {
  return <DefaultExercise {...props} exercise={Exercise} answer={Answer}/>
}

export default Cards
