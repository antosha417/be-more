import React, { ReactNode, useEffect, useRef, useState } from 'react'
import { Card } from "react-bootstrap";
import Progress from "../progress/Progress";
import { isAnswerCorrect } from "../Answer";
import DefaultButtons from "../buttons/DefaultButtons";
import { BasePhrase, DefaultWrapperProps, Language, Phrase } from "../../../contracts";
import { createAudio, playAudio } from "../../../utils/audio";
import { getByName } from "../../../languages/provider";

export const foo = (children: ReactNode) => {
  return children as unknown as JSX.Element;
}

const DefaultExercise = (props: DefaultWrapperProps) => {


  const nextButtonRef = useRef<HTMLButtonElement>(null);

  useEffect(() => {
    nextButtonRef.current && nextButtonRef.current.focus();
  });

  const {groupSize, soundBefore, inProgress, soundAfter, exercise, answer, language, activeGroup, showSentences, phrasesToStudy} = props;

  const [samples, setSamples] = useState<Phrase[]>(() => {
    // todo fix
    return phrasesToStudy.filter(x => activeGroup === '' || x.group === activeGroup)
      .sort(() => 0.5 - Math.random())
      .slice(0, groupSize)
  });

  const [input, setInput] = useState('');
  const [isAnswerShown, setIsAnswerShown] = useState(false);

  const sound = soundBefore || soundAfter;

  if (sound) {
    samples.forEach(createAudio);
  }

  const changeSamples = (activeGroup: string, showSentences: boolean, language: Language, groupSize: number) => {
    const languageClass = getByName(language);
    const newSamples = languageClass.randomSamples(activeGroup, showSentences, groupSize);
    console.log('samples', newSamples)
    sound && newSamples.forEach(createAudio);
    soundBefore && playAudio(newSamples[0]);
    setSamples(newSamples);
    if (sound) {
      // download audio to learn offline if lesson is chosen todo test on mobile
      const words: Phrase[] = getByName(language).words
      words.filter(x => x.group === activeGroup).forEach(createAudio)
    }
  };

  useEffect(() => {
    // changeSamples(activeGroup, showSentences, language, groupSize)
    const languageClass = getByName(language);
    const newSamples = languageClass.randomSamples(activeGroup, showSentences, groupSize);
    console.log('samples', newSamples)
    sound && newSamples.forEach(createAudio);
    soundBefore && playAudio(newSamples[0]);
    setSamples(newSamples);
    if (sound) {
      // download audio to learn offline if lesson is chosen todo test on mobile
      const words: Phrase[] = getByName(language).words
      words.filter(x => x.group === activeGroup).forEach(createAudio)
    }
  }, [activeGroup, showSentences, language, groupSize, soundAfter, soundBefore, sound])

  if (samples.length === 0) {
    changeSamples(activeGroup, showSentences, language, groupSize);
    return null
  }

  const phrase = samples[0]

  const newPhrase = (previous: BasePhrase, phrase: BasePhrase | undefined) => {
    if (previous.audio) {
      previous?.audio.pause();
      previous.audio.currentTime = 0;
    }
    setInput('');
    setIsAnswerShown(false);
    soundBefore && phrase && phrase.audio && phrase.audio.play();
  };

  const nextPhrase = () => {
    phrase?.audio?.pause();
    if (inProgress === 'Cards' || isAnswerCorrect(input, phrase, inProgress)) {
      const newSamples = [...samples.filter(p => p !== phrase)];
      setSamples(newSamples);
      newPhrase(phrase, newSamples[0]);
    } else {
      const newSamples = [...samples.filter(p => p !== phrase), phrase];
      setSamples(newSamples);
      newPhrase(phrase, newSamples[0]);
    }
  };

  const studyAgain = () => {
    const newSamples = [...samples.filter(p => p !== phrase), phrase];
    setSamples(newSamples);
    newPhrase(phrase, newSamples[0]);
  };

  const skipPhrase = () => {
    const newSamples = [...samples.filter(p => p !== phrase)];
    setSamples(newSamples);
    newPhrase(phrase, newSamples[0]);
  };

  const checkPhrase = () => {
    setIsAnswerShown(true);
    soundAfter && playAudio(phrase);
  };

  return (
    <Card>
      <Progress groupSize={groupSize} samplesLen={samples.length}/>
      <Card.Body>
        {exercise({phrase, language, input, setInput, checkPhrase, visible: !isAnswerShown})}
        {answer({phrase, input, language, studyAgain, visible: isAnswerShown})}
        <DefaultButtons isAnswerShown={isAnswerShown}
                        nextPhrase={nextPhrase}
                        nextButtonRef={nextButtonRef}
                        checkPhrase={checkPhrase}
                        skipPhrase={skipPhrase}
                        phrase={phrase}/>
      </Card.Body>
    </Card>
  )
}

export default DefaultExercise