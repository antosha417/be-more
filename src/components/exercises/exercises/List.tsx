import React from 'react';
import { Language, Phrase } from '../../../contracts';
import { Table, Card } from "react-bootstrap";
import MegaphoneButton from "../buttons/MegaphoneButton";
import { createAudio } from "../../../utils/audio";
import Hebrew from "../../../languages/Hebrew";
import { getByName } from "../../../languages/provider";
import { getScore, MAX_SCORES_SIZE } from "../../../utils/statistics";


function List(props: { phrases: Phrase[], isSoundEnabled: boolean, language: Language}) {
  const {phrases, isSoundEnabled} = props
  if (phrases.length < 1)
    return null

  isSoundEnabled && phrases.forEach(createAudio);
  const originalStyle = {textAlign: getByName(props.language).textAlign}
  return (
    <Card>
      <Card.Body>
        <Table striped bordered hover responsive={'sm'}>
          <thead>
          <tr>
            <th> </th>
            <th>Translation</th>
            <th>Original</th>
            <th>Confidence</th>
          </tr>
          </thead>
          <tbody>
          {phrases.map(p =>
            <tr>
              <td>
                {p.audio && <MegaphoneButton phrase={p}/>}
              </td>
              <td>{p.translate}</td>
              <td style={originalStyle}>{Hebrew.isHebrewPhrase(p) ? p.vowel : p.original}</td>
              <td>{(100/MAX_SCORES_SIZE) * getScore(props.language, p.id || '', 0)}%</td>
            </tr>
          )}
          </tbody>
        </Table>
      </Card.Body>
    </Card>
  )
}

export default List
