import React from 'react';
import { Button, ButtonGroup } from "react-bootstrap";


function CheckButton(props: { checkPhrase(): void }) {
  return (
    <ButtonGroup className="mr-2">
      <Button variant="primary" onClick={props.checkPhrase} tabIndex={1}>Check</Button>
    </ButtonGroup>
  )
}

export default CheckButton
