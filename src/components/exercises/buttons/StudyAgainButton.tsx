import React from 'react';
import { Button, ButtonGroup } from "react-bootstrap";


function StudyAgainButton(props: { studyAgain(): void }) {
  return (
    <ButtonGroup className="mr-2">
      <Button variant="primary" onClick={props.studyAgain}>Study again</Button>
    </ButtonGroup>
  )
}

export default StudyAgainButton
