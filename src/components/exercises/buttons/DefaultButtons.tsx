import React from 'react';
import MegaphoneButton from "./MegaphoneButton";
import { Phrase } from "../../../contracts";
import NextButton from "./NextButton";
import SkipButton from "./SkipButton";
import CheckButton from "./CheckButton";

function DefaultButtons(props: { isAnswerShown: boolean, nextPhrase(): void, nextButtonRef: React.RefObject<HTMLButtonElement>, checkPhrase(): void, skipPhrase(): void, phrase: Phrase }) {
  const {phrase, isAnswerShown, checkPhrase, nextButtonRef, nextPhrase, skipPhrase} = props;
  return <>
    {isAnswerShown
      ? <NextButton nextPhrase={nextPhrase} nextButtonRef={nextButtonRef}/>
      : <>
        <CheckButton checkPhrase={checkPhrase}/>
        <SkipButton skipPhrase={skipPhrase}/>
      </>
    }
    {phrase?.audio && <MegaphoneButton phrase={phrase}/>}
  </>
}

export default DefaultButtons