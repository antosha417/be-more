import React from 'react';
import { Button, ButtonGroup } from "react-bootstrap";


function SkipButton(props: { skipPhrase(): void }) {
  return (
    <ButtonGroup className="mr-2">
      <Button variant="secondary" onClick={props.skipPhrase}>Skip</Button>
    </ButtonGroup>
  )
}

export default SkipButton
