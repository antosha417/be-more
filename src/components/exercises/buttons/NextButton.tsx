import React from 'react';
import { ButtonGroup } from "react-bootstrap";


function NextButton(props: { nextPhrase(): void, nextButtonRef: React.RefObject<HTMLButtonElement>}) {
  const {nextPhrase, nextButtonRef} = props;
  return (
    <ButtonGroup className="mr-2">
      <button type="button" className="btn btn-primary" onClick={nextPhrase} ref={nextButtonRef}>Next</button>
    </ButtonGroup>
  )
}

export default NextButton
