import React from 'react';
import { Phrase } from "../../../contracts";
import { ButtonGroup } from "react-bootstrap";


function MegaphoneButton(props: { phrase: Phrase }) {
  return (
    <ButtonGroup className="mr-2">
      <button className='megaphone' onClick={() => props.phrase?.audio?.play()}
              style={{height: '2rem', width: '2rem'}}/>
    </ButtonGroup>
  )
}

export default MegaphoneButton
