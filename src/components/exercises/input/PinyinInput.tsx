import React from 'react';
import { vowelToPinyin, PINYIN_VOWEL_REGEX } from '../../../utils/pinyin'
import { InputGroup, FormControl } from 'react-bootstrap';

function PinyinInput(props: {setInput(input: string): void, checkPhrase(): void, input: string}) {
  return (
    <>
      <InputGroup className="mb-3">
        <FormControl
          size="lg"
          type="text"
          placeholder="pinyin"
          onChange={(e: any) => props.setInput(e.target.value.toLocaleLowerCase().replace(PINYIN_VOWEL_REGEX, vowelToPinyin))}
          onKeyPress={(e: any) => { e.key === 'Enter' && props.checkPhrase() }}
          value={props.input}
          autoFocus={true}
          autoComplete='off'
          autoCorrect='off'
        />
      </InputGroup>
    </>
  )
}

export default PinyinInput
