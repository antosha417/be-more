import React, { useState, useRef } from 'react';
import {
  Button,
  Container,
  Col,
  Row,
  FormControl,
  InputGroup,
} from 'react-bootstrap';
import { HebrewPhrase } from '../../contracts';
import Hebrew from "../../languages/Hebrew";

const TAB_KEYCODE = 9;

function AddHebrew() {

  const [phrases, setPhrases] = useState<HebrewPhrase[]>([]);
  const [sound, setSound] = useState<string>('');
  const [group, setGroup] = useState<string>('');
  const [vowel, setVowel] = useState('');
  const [original, setOriginal] = useState('');
  const [translate, setTranslate] = useState('');
  const [id, setId] = useState<number>(Math.max(...Object.keys(Hebrew.wordsMap).map(x => parseInt(x))) + 1);
  const translationInputRef = useRef<HTMLTextAreaElement>(null);

  const addPhrase = () => {
    setPhrases([{id: id.toString(), original, translate, group, vowel: vowel || '<$>', sound}, ...phrases]);
    setId(x => x + 1)
    setOriginal('');
    setTranslate('');
    setVowel('');
    setSound('');
  };

  const deletePhrase = (phrase: HebrewPhrase) => {
    setPhrases([...phrases.filter(p => p !== phrase)])
  };

  const addPhraseIfEnter = (e: any) => {
    if (e.key === 'Enter') {
      addPhrase();
      translationInputRef?.current?.focus();
    }
  }

  const openLinkOnTab = (e: any, link: string, shortName: string) => {
    if (e.keyCode === TAB_KEYCODE) {
      window.open(link, shortName)?.focus();
    }
  }

  return <>
    <Container className="mt-5">
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="translation"
                         onChange={(e: any) => setTranslate(e.target.value)}
                         value={translate}
                         ref={translationInputRef as React.RefObject<any>}
            />
          </InputGroup>
        </Col>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="hebrew"
                         onChange={(e: any) => setOriginal(e.target.value)}
                         value={original}
                         onKeyDown={(e: any) => {
                           openLinkOnTab(e, 'https://he.wiktionary.org/wiki/' + original, 'wiki')
                           openLinkOnTab(e, 'https://milog.co.il/' + original, 'milog')
                         }}
                         dir={'rtl'}
            />
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col/>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="vowel"
                         onChange={(e: any) => setVowel(e.target.value)}
                         onKeyPress={addPhraseIfEnter}
                         onKeyDown={(e: any) => openLinkOnTab(e, 'https://he.forvo.com/search/' + original, 'sound')}
                         value={vowel}
                         dir={'rtl'}
            />
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="sound"
                         onChange={(e: any) => setSound(e.target.value)}
                         onKeyPress={addPhraseIfEnter}
                         value={sound}
            />
          </InputGroup>
        </Col>
        <Col/>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="group"
                         onChange={(e: any) => setGroup(e.target.value)}
                         onKeyPress={addPhraseIfEnter}
                         value={group}
            />
          </InputGroup>
        </Col>
        <Col/>
      </Row>

      <Button variant="success" onClick={addPhrase}>Add</Button>

      {phrases.map(p =>
        <Row key={p.id}>
          <Col style={{color: 'white'}}>
            {`'${p.id}': {original: '${p.original}', vowel: '${p.vowel}', translate: '${p.translate}', group: '${p.group}', sound: '${p.sound}'},`}
          </Col>
          <Col>
            <Button variant="danger" onClick={() => deletePhrase(p)}>Delete</Button>
          </Col>
        </Row>
      )}
    </Container>
  </>
}

export default AddHebrew;
