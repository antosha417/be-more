import React, { useState, useRef } from 'react';
import {
  Button,
  Container,
  Col,
  Row,
  FormControl,
  InputGroup,
  Dropdown,
  DropdownButton,
  ButtonGroup,
  ButtonToolbar,
} from 'react-bootstrap';
import {ChinesePhrase} from '../../contracts';
import { PINYIN_VOWEL_REGEX, vowelToPinyin } from '../../utils/pinyin';

const TAB_KEYCODE = 9;

function AddChinese() {

  const [phrases, setPhrases] = useState<ChinesePhrase[]>([]);

  const [group, setGroup] = useState<string>('11');
  const [pinyin, setPinyin] = useState('');
  const [original, setOriginal] = useState('');
  const [translate, setTranslate] = useState('');
  const [sound, setSound] = useState('');
  const hieroglyphInputRef = useRef<HTMLTextAreaElement>(null);

  const addPhrase = () => {
    setPhrases([{ pinyin, original, translate, sound, group }, ...phrases]);
    setPinyin('');
    setOriginal('');
    setTranslate('');
    setSound('');
  };

  const deletePhrase = (phrase: ChinesePhrase) => {
    setPhrases([...phrases.filter(p => p !== phrase)])
  };

  const openGoogleTranslateOnTab = (e: any) => {
    if (e.keyCode === TAB_KEYCODE) {
      window.open('https://translate.google.com/#view=home&op=translate&sl=zh-CN&tl=ru&text=' + original, 'translate')?.focus();
    }
  }

  return <>
    <Container className="mt-5">
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="hieroglyph"
              onChange={(e: any) => setOriginal(e.target.value)}
              value={original}
              onKeyDown={openGoogleTranslateOnTab}
              ref={hieroglyphInputRef as React.RefObject<any>}
            />
          </InputGroup>
        </Col>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text"
              placeholder="pinyin" 
              value={pinyin}
              onChange={(e: any) => setPinyin(e.target.value.toLocaleLowerCase().replace(PINYIN_VOWEL_REGEX, vowelToPinyin))}
            />
          </InputGroup>
        </Col>
      </Row>
      <Row>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="translation"
              onChange={(e: any) => setTranslate(e.target.value)}
              value={translate}
            />
          </InputGroup>
        </Col>
        <Col>
          <InputGroup className="mb-3">
            <FormControl size="lg" type="text" placeholder="sound"
              onChange={(e: any) => setSound(e.target.value)}
              onKeyPress={(e: any) => {
                if (e.key === 'Enter') {
                  addPhrase();
                  hieroglyphInputRef?.current?.focus();
                }
              }}
              value={sound}
            />
          </InputGroup>
        </Col>
      </Row>

      <ButtonToolbar aria-label="Toolbar with button groups">
        <ButtonGroup className="mr-2" aria-label="First group">
          <DropdownButton id="lesson-dropdown" title={"Lesson" + group}>
            {Array.from(Array(20).keys()).map(x =>
              <Dropdown.Item onClick={() => setGroup((x + 1).toString())}>Lesson {x + 1}</Dropdown.Item>
            )}
          </DropdownButton>
        </ButtonGroup>
        <ButtonGroup className="mr-2" aria-label="Second group">
          <Button variant="success" onClick={addPhrase}>Add</Button>
        </ButtonGroup>
      </ButtonToolbar>

      {phrases.map(p =>
        <Row>
          <Col style={{ color: 'white' }}>
            {`'${p.group}/${p.sound}.mp3': {pinyin: '${p.pinyin}', original: '${p.original}', translate: '${p.translate}', group: ${p.group}, sound: '${p.group}/${p.sound}.mp3'},`}
          </Col>
          <Col>
            <Button variant="danger" onClick={() => deletePhrase(p)}>Delete</Button>
          </Col>
        </Row>
      )}
    </Container>
  </>
}

export default AddChinese;
