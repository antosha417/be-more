import React from 'react';
import { Exercise, Language, Phrase } from '../contracts';
import NavBar from "./NavBar";
import { useStateWithLocalStorage } from "../utils/localstorage";
import { getByName } from "../languages/provider";
import Cards from "./exercises/exercises/Cards";
import Original from "./exercises/exercises/Original";
import List from "./exercises/exercises/List";
import { Container } from "react-bootstrap";
import Pinyin from "./exercises/exercises/Pinyin";
import Hebrew from "../languages/Hebrew";


function BeMore() {
  const [language, setLanguage] = useStateWithLocalStorage<Language>('language', 'Hebrew');
  const [inProgress, setInProgress] = useStateWithLocalStorage<Exercise>('inProgress', Hebrew.exercises[0]);
  const [activeGroup, setActiveGroup] = useStateWithLocalStorage<string>('activeGroup', '');
  const [soundBefore, setSoundBefore] = useStateWithLocalStorage<boolean>('soundBefore', false);
  const [soundAfter, setSoundAfter] = useStateWithLocalStorage<boolean>('soundAfter', true);
  const [showSentences, setShowSentences] = useStateWithLocalStorage<boolean>('showSentences', true);
  const [groupSize, setGroupSize] = useStateWithLocalStorage<number>('groupSize', 5);

  const words: Phrase[] = getByName(language).words;
  const phrasesToStudy = words.filter((x: Phrase) => activeGroup === '' || x.group === activeGroup);

  const changeLanguage = (language: Language) => {
    const languageClass = getByName(language)
    const newExercise = languageClass.isAvailable(inProgress) ? inProgress : languageClass.exercises[0]
    const newGroup = languageClass.groups.includes(activeGroup) ? activeGroup : ''
    setInProgress(newExercise);
    setActiveGroup(newGroup);
    setLanguage(language);
  }

  const state = {
    phrasesToStudy,
    language, changeLanguage,
    activeGroup, setActiveGroup,
    inProgress, setInProgress,
    soundBefore, setSoundBefore,
    soundAfter, setSoundAfter,
    showSentences, setShowSentences,
    groupSize, setGroupSize
  }

  return (
    <>
      <NavBar {...state}/>

      <Container className="mt-5">
        {inProgress === 'Cards' && <Cards {...state} />}
        {inProgress === 'Original' && <Original {...state}/>}
        {inProgress === 'Pinyin' && <Pinyin {...state}/>}
        {inProgress === 'List' &&
        <List {...state} phrases={phrasesToStudy} isSoundEnabled={soundBefore || soundAfter}/>}
      </Container>
    </>
  );
}

export default BeMore;
