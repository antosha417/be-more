import React from 'react';
import { languages, NavBarProps, Exercise } from '../contracts';
import { NavDropdown, Navbar, Nav } from 'react-bootstrap';
import { getByName } from "../languages/provider";

function NavBar(props: NavBarProps) {
  const {
    language, changeLanguage, activeGroup, setActiveGroup, inProgress, setInProgress,
    showSentences, setShowSentences, soundBefore, setSoundBefore, soundAfter, setSoundAfter, groupSize, setGroupSize
  } = props;
  const languageClass = getByName(language)
  const exercises: Exercise[] = languageClass.exercises
  const groups = languageClass.groups
  return (
    <>
      <Navbar bg="light" expand="lg" variant="light">
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <NavDropdown id="dropdown-basic-lang" title={language}>
            {languages.map(x =>
              <NavDropdown.Item onClick={() => changeLanguage(x)} active={language === x}
                                key={x}>{x}</NavDropdown.Item>)}
          </NavDropdown>
          <NavDropdown id="dropdown-basic-lesson" title={activeGroup === '' ? "All Words" : 'Lesson: ' + activeGroup}>
            <NavDropdown.Item
              onClick={() => setActiveGroup('')}
              active={activeGroup === ''}>
              All Words
            </NavDropdown.Item>
            {groups.map(g => <NavDropdown.Item onClick={() => setActiveGroup(g)}
                                               active={activeGroup === g}
                                               key={g}> {g} </NavDropdown.Item>)}
          </NavDropdown>
          <NavDropdown id="dropdown-basic-group-size" title={'Size: ' + groupSize}>
            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(g => <NavDropdown.Item onClick={() => setGroupSize(g)}
                                                                        active={groupSize === g}
                                                                        key={g}> {g} </NavDropdown.Item>)}
          </NavDropdown>
          <NavDropdown id="dropdown-basic-exercise" title={inProgress}>
            {exercises.map(e => <NavDropdown.Item onClick={() => setInProgress(e)}
                                                  key={e}> {e} </NavDropdown.Item>)}
          </NavDropdown>
          <Nav.Link
            onClick={() => setShowSentences(!showSentences)}>{'Sentences' + (showSentences ? 'On' : 'Off')}</Nav.Link>
          <Nav.Link
            onClick={() => setSoundBefore(!soundBefore)}>{'Sound Before ' + (soundBefore ? 'On' : 'Off')}</Nav.Link>
          <Nav.Link onClick={() => setSoundAfter(!soundAfter)}>{'Sound After ' + (soundAfter ? 'On' : 'Off')}</Nav.Link>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
}

export default NavBar;
