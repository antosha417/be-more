import requests
import sys
import re


def get_original_word(hebrew_phrase):
    m = re.search("{original: '(.*?)',", hebrew_phrase)
    if m is not None:
        return m.group(1)
    return ''


def get_vowel_word(original_word):
    r = requests.post(url, headers=headers, data=f"txt= {original_word}&usr=&pass=&ktivMale=false".encode('utf-8'))
    m = re.search('<Result>(.*)</Result>', r.text)
    if m is not None:
        return m.group(1)
    print('Cannot find vowel-word')
    print(f'responce status code: {r.status_code}')
    print(f'responce text: {r.text}')
    return '<$>'


if __name__ == '__main__':
    headers = {
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36',
        "Accept": "*/*",
        "Accept-Language": "en-US,en;q=0.9,ru;q=0.8",
        "Content-Type": "application/x-www-form-urlencoded",
        'Referer': 'http://www.nakdan.com/Nakdan.aspx',
        'Origin': 'http://www.nakdan.com',
        'Cookie': 'ASP.NET_SessionId=rf5tgefz4ledijgbjtb1ojq5; __gads=ID=e278492bd7dcb4f0:T=1600795203:S=ALNI_MZx5cOtsZQhutB8n204NsWRE_zxDQ',
    }

    url = "http://www.nakdan.com/GetResult.aspx"

    # for nc -kl 8765
    # lurl = "http://localhost:8765"
    lines = sys.stdin.read().strip().splitlines()
    print('5 минут, Турецкий!')
    for line in lines:
        if "vowel: '<$>'" not in line:
            print(line)
            continue

        word = get_original_word(line)
        print(line.replace('<$>', get_vowel_word(word)))
