import { Phrase } from "../contracts";

export const createAudio = (phrase: Phrase) => {
  if (phrase.sound && !phrase.audio) {
    phrase.audio = new Audio('/sounds/' + phrase.sound);
  }
};

export const playAudio = (phrase: Phrase) => {
  if (!phrase.audio) {
    return;
  }
  phrase.audio.currentTime = 0;
  // console.log(JSON.stringify(phrase), phrase.audio.currentTime);
  phrase.audio.play().then();
};

