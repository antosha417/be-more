import errno
import os
import sys
import re
import cloudscraper


def get_original_word(hebrew_phrase):
    m = re.search("{original: '(.*?)',", hebrew_phrase)
    if m is not None:
        return m.group(1)
    return ''


def get_sound_url(hebrew_phrase):
    m = re.search("sound: '(.*?)'", hebrew_phrase)
    if m is not None:
        return m.group(1)
    return ''


def get_id(hebrew_phrase):
    m = re.search("'(.*?)': {", hebrew_phrase)
    if m is not None:
        return m.group(1)
    return ''


def get_group(hebrew_phrase):
    m = re.search("group: '(.*?)'", hebrew_phrase)
    if m is not None:
        return m.group(1)
    return ''


def create_dirs(file):
    if not os.path.exists(os.path.dirname(file)):
        try:
            os.makedirs(os.path.dirname(file))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise


if __name__ == '__main__':
    print('Введити фразы:')
    lines = sys.stdin.read().strip().splitlines()
    print('5 минут, Турецкий!\n')
    scraper = cloudscraper.create_scraper()
    for line in lines:
        sound_url = get_sound_url(line)
        if len(sound_url) == 0:
            print(line)
            continue
        phrase_id = get_id(line)
        group = get_group(line)

        new_url = 'hebrew/' + group + '/' + phrase_id + '.mp3'
        filename = '../../public/sounds/' + new_url
        create_dirs(filename)

        content = scraper.get(sound_url).content
        with open(filename, 'wb') as f:
            f.write(content)

        print(re.sub("sound: '(.*?)'", f"sound: '{new_url}'", line))
