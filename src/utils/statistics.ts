import { Language } from "../contracts";

// `MAX_SCORES_SIZE` is how much attempts is stored in local storage
export const MAX_SCORES_SIZE = 20
// if more than `LOVER_BOUND` attempts is correct, then word will appear in samples less often
export const LOVER_BOUND = 13

const getKey = (language: Language, phraseId: string) => `score/${language}/${phraseId}`;
const getFromStorage = (key: string) => JSON.parse(localStorage.getItem(key) || '[]');

export const saveScoreInLocalStorage = (language: Language, phraseId: string, isCorrect: boolean) => {
  const key = getKey(language, phraseId);
  try {
    const scores = getFromStorage(key)
    localStorage.setItem(key, JSON.stringify([isCorrect, ...(scores || [])].slice(0, MAX_SCORES_SIZE)))
  } catch (e) {
    console.log(e);
    localStorage.setItem(key, '[]')
  }
}

export const getScore = (language: Language, phraseId: string, lover_bound: number) => {
  const key = getKey(language, phraseId);
  try {
    const scores = getFromStorage(key)
    return Math.max(0, scores.filter((x: boolean) => x).length - lover_bound)
  } catch (e) {
    console.log(e)
  }
  return 0;
}
