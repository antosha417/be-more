
export type Language = 'Chinese' | 'Hebrew';
export const languages: Language[] = ['Chinese', 'Hebrew']

export type Exercise = ChineseExercise | HebrewExercise;
export type ChineseExercise = 'Cards' | 'Pinyin' | 'Original';
export type HebrewExercise = 'Cards' | 'Original' | 'List';

export type Phrase = ChinesePhrase | HebrewPhrase


export interface BasePhrase {
  original: string,
  translate: string,
  group: string,
  sound?: string,
  audio?: HTMLAudioElement,
  relatedIds?: string[],
  //todo make required
  id?: string,
}

export interface ChinesePhrase extends BasePhrase {
  pinyin: string,
}

export interface HebrewPhrase extends BasePhrase {
  vowel: string,
  id: string,
}

export type NavBarProps = {
  language: Language,
  changeLanguage(language: Language): void,
  activeGroup: string,
  setActiveGroup(group: string): void,
  inProgress: Exercise,
  setInProgress(e: Exercise): void,
  soundBefore: boolean,
  setSoundBefore(s: boolean): void,
  soundAfter: boolean,
  setSoundAfter(s: boolean): void,
  showSentences: boolean,
  setShowSentences(s: boolean): void,
  groupSize: number,
  setGroupSize(group: number): void,
}

export type CardState = CardProps

export type CardProps = {
  showSentences: boolean,
  activeGroup: string,
  language: Language,
  phrasesToStudy: Phrase[],
  inProgress: Exercise,
  soundBefore: boolean,
  soundAfter: boolean,
  groupSize: number,
}

export type DefaultWrapperProps = {
  showSentences: boolean,
  activeGroup: string,
  exercise(props: any): JSX.Element,
  answer(props: any): JSX.Element,
  phrasesToStudy: Phrase[],
  soundBefore: boolean,
  soundAfter: boolean,
  language: Language,
  inProgress: Exercise,
  groupSize: number,
}

// todo
export type ExercisesProps = OriginalExerciseProps

export type OriginalExerciseProps = {
  phrase: Phrase,
  language: Language
  input: string,
  setInput(input: string): void,
  checkPhrase(): void,
}