import { BasePhrase, Language } from "../contracts";
import { getScore, LOVER_BOUND } from "../utils/statistics";

function sampleRelated<T extends BasePhrase>(word: T, phrasesMap: { [id: string]: T }): T[] {
  return (word.relatedIds || [])
    .map(id => phrasesMap[id])
    .filter(id => !!id)
    .sort(() => 0.5 - Math.random())
    .slice(0, 1)
}

export function randomSamples<T extends BasePhrase>(
  language: Language,
  activeGroup: string,
  showRelatedSentences: boolean,
  showRelatedWords: boolean,
  words: T[],
  wordsMap: { [id: string]: T },
  phrasesMap: { [id: string]: T },
  groupSize: number): T[] {
  const randomWords = words.filter(x => activeGroup === '' || x.group === activeGroup)
    .sort(() => 0.5 - Math.random())
    .sort((a, b) =>
      getScore(language, a.id || '', LOVER_BOUND) - getScore(language, b.id || '', LOVER_BOUND))
    .flatMap(w => [w, ...(showRelatedWords ? sampleRelated(w, wordsMap) : [])])
    .slice(0, groupSize)

  // todo think about sentences
  //  seems that it is not useful to store two separate lists cause ideally id should be unique
  const randomWordsWithSentences = randomWords
    .flatMap(w => [w, ...sampleRelated(w, phrasesMap)])
    .slice(0, groupSize);
  return showRelatedSentences ? randomWordsWithSentences : randomWords;
}